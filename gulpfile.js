var gulp = require('gulp');
var sass = require('gulp-sass');
var compass = require('gulp-compass');
var autoprefixer = require('gulp-autoprefixer');
var watch = require('gulp-watch');
var usemin = require('gulp-usemin');
var uglify = require('gulp-uglify');
var minifyHtml = require('gulp-minify-html');
var minifyCss = require('gulp-minify-css');
var rev = require('gulp-rev');
var imagemin = require('gulp-imagemin');
var browserSync = require('browser-sync').create();
var reload      = browserSync.reload;

// Compiling SCSS using SASS/Compass
gulp.task('styles', function() { 
    return gulp.src(['assets/scss/*.scss', '!sass/_*.scss'])
    .pipe(compass({
        config_file: 'config.rb',
        css: 'assets/css',
        sass: 'assets/scss',
        image: 'assets/images',
        sourcemap: true
    }))
    .pipe(autoprefixer('last 2 version', 'safari 5', 'ie 7', 'ie 8', 'ie 9', 'opera 12.1', 'ios 6', 'android 4'))
    .pipe(gulp.dest('assets/css'));
});

// Watch
gulp.task('watch', ['styles'], function () {
    gulp.watch('*.html').on("change", reload);
    gulp.watch('assets/scss/**/*.scss', ['styles']);
    gulp.watch('assets/css/**/*.css').on("change", reload);
    gulp.watch('assets/js/**/*.js').on("change", reload);
});

// use default task to launch Browsersync and watch JS files
gulp.task('serve', ['styles'], function () {

    // Serve files from the root of this project
    browserSync.init({
        server: {
            baseDir: "./"
        }
    });
});

//Build
gulp.task('usemin', function() {
    return gulp.src('./*.html')
        .pipe(usemin({
            css: [ rev ],
            html: [ function () {return minifyHtml({ empty: true });} ],
            js: [ uglify, rev ],
            inlinejs: [ uglify ],
            inlinecss: [ minifyCss, 'concat' ]
        }))
    .pipe(gulp.dest('build/'));
});


gulp.task('default', ['serve', 'styles', 'watch']);
//gulp.task('build', ['usemin', 'images', 'fonts']);

