(function(window, document) {
    "use strict";

    /* como usar
        var carousel = new Carousel({
            pagination: $(".carousel__nav-dots"),
            container: $(".carousel__inner"),
            effect: 'fade',
            speed: .5,
            time: 5,
            clickable: false,
            rotate: true,
            nextBtn: $('.carousel__nav-next'),
            prevBtn: $('.carousel__nav-prev')
        });
    */

    var Carousel = function(opts) {
        var options = {
            effect: 'fade',
            speed: .5,
            time: 3,
            clickable: true,
            rotate: false,
            pagination: null,
            nextBtn: null,
            prevBtn: null
        };

        $.extend(options, opts);

        var pagination = options.pagination;
        var pagintaionItems = pagination.children();
        var container = options.container;
        var containerItems = container.find(".item");
        var nextBtn = options.nextBtn;
        var prevBtn = options.prevBtn;
        var containerSize = { w: container.width(), h: container.height() };
        var currentItem = 0;
        var nextItem = 1;
        var totalItems = containerItems.length;
        var timeout = null;

        repositionItems();
        setupPagination();
        addTimeout();
        addPrevNext();

        function addTimeout () {
            timeout = window.setTimeout(function () {
                changeSlide("next");

                if (pagination != null) {
                    pagintaionItems.removeClass("active");
                    pagintaionItems.eq(nextItem).addClass("active");
                }
                
            }, Math.floor(options.time * 1000));
        }

        // Reposiciona os items de acordo com o 'effect' fade ou slide 
        function repositionItems () {
            if (options.effect == "slide") {
                containerItems.css({ left : containerSize.w });
                containerItems.eq(currentItem).css({ left : 0 }).addClass("active");
            } else if (options.effect == "fade") {
                containerItems.css({ opacity : 0, zIndex: 0 });
                containerItems.eq(currentItem).css({ opacity : 1, zIndex: 1 }).addClass("active");
            }
        }

        // adiciona a paginacao 
        function setupPagination () {
            if (pagination != null) {
                pagintaionItems.each(function (i) {
                    $(this).attr("data-index", i);

                    $(this).click(function (event) {
                        event.preventDefault();
                        var id = parseInt($(this).attr("data-index"));

                        window.clearTimeout(timeout);

                        if (pagination != null) {
                            pagintaionItems.removeClass("active");
                            pagintaionItems.eq(id).addClass("active");
                        }
                        
                        changeSlide(id);
                    });
                });
            }
            
        }

        function addPrevNext () {
            
            if (nextBtn != null) {
                nextBtn.on('click', function(event) {
                    event.preventDefault();
          
                    window.clearTimeout(timeout);
                    changeSlide('next');
                });
            }

            if (prevBtn != null) {
                prevBtn.on('click', function(event) {
                    event.preventDefault();

                    window.clearTimeout(timeout);
                    changeSlide('prev');
                });
            }
        }

        // muda para o proximo slide ou anterior
        function changeSlide (dir) {
            if (dir === "next")  {
                nextItem = currentItem + 1;

                if (nextItem > totalItems - 1)
                {
                    nextItem = 0;
                }
            } else if (dir === "prev") {
                nextItem = currentItem - 1;

                if (nextItem < 0)
                {
                    nextItem = totalItems - 1;
                }
            } else {
                nextItem = dir;
            }

            animate (dir);
        }

        // faz a animaÃ§Ã£o de acordo com o 'effect' fade ou slide
        function animate (dir) {
            var time = Math.floor (options.speed * 1000);

            if (dir !== "next" && dir !== "prev")
            {
                dir = (dir > currentItem) ? "next" : "prev";
            }

            window.clearTimeout(timeout);

            switch (dir) {
                case "next":
                    if (options.effect == "slide") {
                        // Posiciona o proximo item para a animaÃ§Ã£o
                        containerItems.eq(nextItem).css({ left : containerSize.w });

                        // animaÃ§Ã£o de fade do proximo slide
                        containerItems.eq(currentItem).stop().animate({ left : -containerSize.w }, time);
                    } else if (options.effect == "fade") {
                        containerItems.eq(nextItem).css({ opacity : 0, zIndex: 0 }, time);
                        containerItems.eq(currentItem).stop().animate({  opacity : 0, zIndex: 0 }, time);
                    }
                break;

                case "prev":
                    if (options.effect == "slide") {
                        // Posiciona o proximo item para a animaÃ§Ã£o
                        containerItems.eq(nextItem).css({ left : -containerSize.w });

                        // animaÃ§Ã£o de fade do proximo slide
                        containerItems.eq(currentItem).stop().animate({ left : containerSize.w }, time);
                    } else if (options.effect == "fade") {
                        containerItems.eq(nextItem).css({ opacity : 0, zIndex: 0}, time);
                        containerItems.eq(currentItem).stop().animate({  opacity : 0, zIndex: 0 }, time);
                    }
                break;
            }

            if (options.effect == "slide") {
                // animaÃ§Ã£o de saido do slide anterior
                containerItems.eq(nextItem).stop().animate({ left : 0 }, time, function () {
                    currentItem = nextItem;
                    containerItems.removeClass("active").eq(currentItem).addClass("active");

                    addTimeout ();
                });
            } else if (options.effect == "fade") {
                containerItems.eq(nextItem).stop().animate({ opacity : 1, zIndex: 1 }, time, function () {
                    currentItem = nextItem;
                    containerItems.css({ opacity : 0, zIndex: 0}, time);
                    containerItems.removeClass("active").eq(currentItem).addClass("active");
                    containerItems.eq(currentItem).css({ opacity : 1, zIndex: 1}, time);

                    addTimeout ();
                });
            }

        }
    };


    window.Carousel = Carousel;
})(window, document);